import React, { useEffect, useState } from 'react'
import axios from 'axios'
import List from './components/listofCountries'
import './App.css'

function App() {

  const[allCountries, setallCountries] = useState(null)
useEffect (()=>{
  axios.get('https://restcountries.eu/rest/v2/all')
  .then(Response =>{
    console.log(Response.data) 
    setallCountries(Response.data)
  })
}, [])
  return (
    <>
    <h1>List of All Counties in the world</h1>
    {
      allCountries?<div>
        {
          allCountries.map(country=>{
            return <List key ={country.id} country={country} />
          })
        }
        <List/> 
      </div> : <div><span>Loading...</span></div>
    }
   
  </>
  
  )
}

export default App
